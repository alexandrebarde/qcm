$(document).ready(function() {
    $("button[id^='publishQcm']").click(function() {
        publishQcm(this);
    });
});

function publishQcm(index) {
    var idPublishQcm = jQuery(index).val();


    $.ajax({
        //On utilise le type GET
        type: 'POST',

        //Ici on appelle la fonction CreateQcm du controleur QuestionReponseControlleur.php
        url: "showQcms",

        //On passe en paramètre le message et l'auteur du message
        data: {
            publishQcm: idPublishQcm
        },

        // code_html contient le HTML renvoyé
        success : function(code_html, statut){
            $("#createdMessage" + idPublishQcm).text("Le qcm n°" + idPublishQcm + " est maintenant publié.");
            $("#createdMessage" + idPublishQcm).attr('class', 'text-success');
        }
    });

}