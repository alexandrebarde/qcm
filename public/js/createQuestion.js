$(document).ready(function() {
    $("#createQuestion").click(function() {
        createQuestion();
    });
});

function createQuestion() {
    var theme = $("#theme").val();
    var content = $("#content").val();
    var response = [];
    var responseTrueFalse = [];
    var i = 1;

    while($("#response" + i).val() != undefined) {
        response.push($("#response" + i).val())
        responseTrueFalse.push($("#response" + i + "TrueFalse").is(':checked'));
        i++;
    }

    $.ajax({
        //On utilise le type GET
        type: 'POST',

        //Ici on appelle la fonction CreateQcm du controleur QuestionReponseControlleur.php
        url: "createQuestionToDB",

        //On passe en paramètre le message et l'auteur du message
        data: {
            theme: theme,
            content: content,
            response: response,
            responseTrueFalse: responseTrueFalse
        },

        // code_html contient le HTML renvoyé
        success : function(code_html, statut){
            var nbResponses = $("input[id*='response']").length/2;
            $("#createdMessage").text("Question créée avec succès");
            $("#createdMessage").attr('class', 'text-success');
            for(var i = 0; i <= nbResponses; i++) {
                $("#response" + i).val("");
                $("#response" + i + "TrueFalse").prop('checked', false);
            }
            $("#content").val("");
        }
    });

}