<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\User;
use App\Entity\Question;
use App\Entity\QuestionList;
use App\Entity\Response;
use App\Entity\Theme;




class QuestionController extends AbstractController
{

    /**
     * Redirection sur la page d'accueil
     * @Route("/updateQuestion/")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectUpdateQuestion()
    {
        return $this->redirect('/');
    }

    /**
     * Redirection sur la page d'accueil
     * @Route("/showQuestion/")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectShowQuestion()
    {
        return $this->redirect('/');
    }

    /**
     * Affiche l'ensemble des questions et met à jour les questions/reponses
     * @Route("/showQuestions", name="showQuestions")
     */
    public function afficherQuestions(Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session))
        {
            // Si l'utilisateur connecté est un enseignant
            if($request->getSession()->get('role') == 'Enseignant') {

                $repository = $this->getDoctrine()->getRepository(Question::class);
                $repositoryTheme = $this->getDoctrine()->getRepository(Theme::class);

                // Modification d'une question/réponse lorsque l'utilisateur clique sur submit depuis la route "/updateQuestion/{idQuestion}"
                if($request->get('question'))
                {
                    $entityManager = $this->getDoctrine()->getManager();
                    $question = $entityManager->getRepository(Question::class)->find($request->get('question'));
                    $responseRequest = $request->get('idResponse');
                    $responsesAnswerRequest = $request->get('idResponsesAnswer');

                    //Modification de la nouvelle question
                    $question->setContent($request->get('updatedQuestion'));
                    //Modification du nouveau theme
                    if(count($repositoryTheme->findBy(['content' => $request->get('updatedTheme')])) == 0) {
                        $newTheme = new Theme();
                        $newTheme->setContent($request->get('updatedTheme'));
                        $question->setTheme($newTheme);
                    }

                    //Modification des nouvelles réponses
                    $i = 0;
                    while($request->get('response' . $i)) {
                        // Création de la réponse
                        $response = new Response();
                        $response->setContent($request->get('response' . $i));
                        $response->setRightanswer($request->get('response' . $i . 'TrueFalse') == 'true' ? 1 : 0);

                        $question->addResponse($response);
                        $entityManager->persist($response);
                        $i++;
                    }
                    // Mise à jour des reponses
                    foreach($responseRequest as $response)
                    {
                        $entityManager->getRepository(Response::class)->find($response)->setContent($request->get('updatedResponse' . $response));
                    }

                    // Mise à jour de la réponse vraie ou fausse
                    foreach($responsesAnswerRequest as $responseAnswer)
                    {
                        $answer = $request->get('updatedResponsesAnswer' . $responseAnswer);
                        $entityManager->getRepository(Response::class)->find($responseAnswer)->setRightAnswer(isset($answer));

                    }
                    $requetes = $request->request->all();
                    $i = count($requetes);
                    $j = 1;
                    while($j <= $i)
                    {
                        if(array_key_exists("response" . $j, $requetes)) {
                            $responseRightAnswer = $request->get('response' . $j . 'TrueFalse');
                            // Création et ajout de la réponse
                            $response = new Response();
                            $response->setContent($request->get('response' . $j));
                            $response->setRightanswer(isset($responseRightAnswer));
                            $question->addResponse($response);
                            $entityManager->persist($response);
                        }
                        $j++;
                    }
                    $entityManager->flush();

                    $userRole = $request->getSession()->get('role');
                    $questions = $repository->findAll();
                    return $this->render('/teacher/showQuestions.html.twig', [
                        "questions" => $questions,
                        "userRole" => $userRole
                    ]);

                    // Lorsqu'il n'y a aucune requête, on affiche simplement la liste des questions
                } else {
                    $userRole = $request->getSession()->get('role');
                    $repository = $this->getDoctrine()->getRepository(Question::class);
                    $questions = $repository->findAll();
                    return $this->render('/teacher/showQuestions.html.twig', [
                        "questions" => $questions,
                        "userRole" => $userRole
                    ]);
                }
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }

        } else {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * Affichage des questions/réponses à modifier
     * @Route("/updateQuestion/{idQuestion}", name="updateQuestion")
     * @param int $idQuestion
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateQuestion($idQuestion, Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session))
        {
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(Question::class);
                $question = $repository->find($idQuestion);
                $questionResponse = array("questionsResponses" => $question);
                return $this->render('/teacher/updateQuestion.html.twig', $questionResponse);
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }

    }

    /**
     * Affichage d'une question
     * @Route("/showQuestion/{idQuestion}", name="showQuestion")
     * @param int $idQuestion
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showQuestion($idQuestion, Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session))
        {
            // Si l'utilisateur est un enseignant
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(Question::class);
                $question = $repository->find($idQuestion);
                $questionResponse = array("questionsResponses" => $question);
                return $this->render('/teacher/showQuestion.html.twig', $questionResponse);
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }

    }


    /**
     * Affiche les questions en fonction de ce que l'utilisateur a rentré
     * @Route("/showQuestionTheme", name="showQuestionTheme")
     */
    public function showQuestionTheme(Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session))
        {
            // Si l'utilisateur est un enseignant
            if($request->getSession()->get('role') == 'Enseignant') {
                $theme = $request->get('theme');

                $repTheme = $this->getDoctrine()->getRepository(Theme::class);
                $idTheme = $repTheme->findByContent($theme);

                $repQuestions = $this->getDoctrine()->getRepository(Question::class);
                $questions = $repQuestions->findBy(['theme' => $idTheme]);

                return $this->render('/teacher/showQuestions.html.twig', [
                    "questions" => $questions
                ]);
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * Vérifie l'identifiant et le mot de passe fournit dans le formulaire
     * @Route("/createQuestion", name="createQuestion")
     */
    public function formCreateQuestion(Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session)) {
            $userRole = $request->getSession()->get('role');

            if($request->getSession()->get('role') == 'Enseignant')
            {
                $repTheme = $this->getDoctrine()->getRepository(Theme::class);
                $themes = $repTheme->findAll();
                $userInf =
                    array(
                        'username' => $session->get('username'),
                        'password' => $session->get('password'),
                        'createdMessage' => 'Aucune question n\'a été créée pour le moment',
                        'themes' => $themes);
                return $this->render('/teacher/createQuestion.html.twig', $userInf);
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }

        }
        else {
            $erreur['erreur'] = "Veuillez vous authentifier.";
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * Vérifie l'identifiant et le mot de passe fournit dans le formulaire
     * @Route("/createQuestionToDB", name="createQuestionToDB")
     */
    public function createQuestionToDB(Request $request) {
        $session = new Session();

        // Si la session existe
        if(User::checkSession($request, $session))
        {
            /** @var User $user */
            if($request->getSession()->get('role') == 'Enseignant') {
                $user = $this->getDoctrine()->getRepository(User::Class)->findOneByLogin($session->get('username'));
                $entityManager = $this->getDoctrine()->getManager();

                // Récupération des informations de la question
                $theme = $request->get('theme');
                /** @var Theme $themeDB */
                $themeDB = $this->getDoctrine()->getRepository(Theme::class)->find($theme);
                $content = $request->get('content');
                if($theme && $content) {

                    // Création de la question
                    $question = new Question();
                    $question->setContent($content);
                    $question->setAuthor($user);
                    $question->setTheme($themeDB);

                    $entityManager->persist($question);

                    //On crée une réponse pour chaque input response present dans le formulaire
                    $responseTmp = $request->get('response');
                    $responseTrueFalseTmp = $request->get('responseTrueFalse');
                    foreach($responseTrueFalseTmp as $key => $value) {

                        // Récupération des informations de la requête
                        $responseRightAnswer = $responseTrueFalseTmp[$key];

                        // Création de la réponse
                        $response = new Response();
                        $response->setContent($responseTmp[$key]);
                        $response->setRightanswer($responseRightAnswer == 'true' ? 1 : 0);

                        $question->addResponse($response);
                        $entityManager->persist($response);
                    }
                    $entityManager->flush();

                    // on stock les informations à afficher à l'utilisateur
                    $userInf['createdMessage'] = 'Question créée avec succès';
                    $userInf['themes'] = $theme;
                    return $this->render('/teacher/createQuestion.html.twig', $userInf);
                }
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier.");
            return $this->render('home.html.twig', $erreur);
        }
    }


    /**
     * @Route("/deleteQuestion/{idQuestion}", name="deleteQuestion")
     * @param int $idQuestion
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteQuestion($idQuestion, Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session))
        {
            // Si l'utilisateur est un enseignant
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(Question::class);
                $repositoryQuestionList = $this->getDoctrine()->getRepository(QuestionList::class);

                // Si la question qu'on essaie de supprimée est utilisée dans un qcm
                if(sizeof($repositoryQuestionList->findBy(['question_id' => $idQuestion])) > 0) {

                    // On renvoi un message indiquant que la question n'a pas été supprimée puis on redirige
                    $this->addFlash('danger', 'Question non supprimée car utilisée dans un qcm!');
                    return $this->redirect('/showQuestions');

                // Si la question n'était pas utilisée dans un qcm, alors on la supprime
                } else {
                    $question = $repository->find($idQuestion);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($question);
                    $entityManager->flush();
                    $this->addFlash('success', 'Question supprimée!');
                    return $this->redirect('/showQuestions');
                }
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }
}