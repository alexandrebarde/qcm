<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\User;
use App\Entity\Question;
use App\Entity\QuestionList;
use App\Entity\Qcm;
use App\Entity\Response;
use App\Entity\Result;



class QcmController extends AbstractController
{

    /**
     * Affiche l'ensemble des qcm
     * @Route("/showQcms", name="afficherQcms")
     */
    public function afficherQcm(Request $request)
    {
        $session = new Session();

        // Si la session existe
        if (User::checkSession($request, $session)) {

            // Si un qcm a été publié
            if($request->get('publishQcm')) {
                $entityManager = $this->getDoctrine()->getManager();

                // Recuperation du qcm qui a ete publie, et on le rend visible aux etudiants
                $qcm = $entityManager->getRepository(Qcm::class)->find($request->get('publishQcm'));
                $qcm->setVisible(1);
                $entityManager->flush();

            // Si aucun qcm n'a ete publie
            } else {

                // Si on est etudiant
                if($request->getSession()->get('role') == 'Etudiant') {
                    $repositoryQcms = $this->getDoctrine()->getRepository(Qcm::class);
                    $repositoryUser = $this->getDoctrine()->getRepository(User::Class);
                    $repositoryResult = $this->getDoctrine()->getRepository(Result::class);

                    $idUser = $repositoryUser->find($request->getSession()->get('idUser'));

                    // Recuperation dans la base, de tous les qcm qui sont visibles
                    $qcms = $repositoryQcms->findBy(
                        ['visible' => '1']
                    );

                    $arrayAnsweredQuestions = array();
                    foreach($qcms as $key => $value) {

                        // Recuperation dans un array, des qcms que l'etudiant a deja repondu
                        if($repositoryResult->findBy(['user_id' => $idUser, 'qcm_id' => $value->getId()])) {
                            array_push($arrayAnsweredQuestions, $qcms[$key]);
                        }

                        // On retire de $qcm, tout les qcm dont la date limite de rendu est inferieure à la date d'aujourd'hui
                        if($value->getDeadline() < new \DateTime() || $repositoryResult->findBy(['user_id' => $idUser, 'qcm_id' => $value->getId()])) {
                            unset($qcms[$key]);
                        }
                    }
                    $createdMessage = "";
                    $userRole = $request->getSession()->get('role');
                    return $this->render('/teacher/showQcms.html.twig', [
                        "qcms" => $qcms,
                        "createdMessage" => $createdMessage,
                        'userRole' => $userRole,
                        'answeredQcms' => $arrayAnsweredQuestions
                    ]);
                }
            }

            $repositoryQcms = $this->getDoctrine()->getRepository(Qcm::class);

            $qcms = $repositoryQcms->findAll();
            $createdMessage = "";
            $userRole = $request->getSession()->get('role');
            return $this->render('/teacher/showQcms.html.twig', [
                "qcms" => $qcms,
                "createdMessage" => $createdMessage,
                'userRole' => $userRole
            ]);
        } else {
            $erreur = array('erreur' => "");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * Redirection sur la page d'accueil
     * @Route("/updateQcm/")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectUpdateQcm()
    {
        return $this->redirect('/');
    }

    /**
     * Redirection sur la page d'accueil
     * @Route("/answerQcm/")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectAnswerQcm()
    {
        return $this->redirect('/');
    }

    /**
     * Réponse à un qcm
     * @Route("/answerQcm/{idQcm}", name="answerQcmId")
     */
    public function answerQcmId(Request $request, $idQcm)
    {
        $session = new Session();
        // Si la session existe
        if (User::checkSession($request, $session)) {
            if($request->getSession()->get('role') == 'Etudiant') {
                $repositoryQcms = $this->getDoctrine()->getRepository(Qcm::class);
                $repositoryQuestionList = $this->getDoctrine()->getRepository(QuestionList::class);

                $qcmQuestions = $repositoryQuestionList->findBy(['qcm_id' => $idQcm]);

                $qcm = $repositoryQcms->find($idQcm);
                $createdMessage = "";
                $userRole = $request->getSession()->get('role');

                return $this->render('/student/answerQcm.html.twig', [
                    'qcm' => $qcm,
                    'createdMessage' => $createdMessage,
                    'userRole' => $userRole,
                    'qcmQuestions' => $qcmQuestions
                ]);
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        } else {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }
    /**
     * Fonction de notation de l'etudiant lorsqu'il repond a un qcm
     * @Route("/answerQcm", name="answerQcm")
     */
    public function answerQcm(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryQuestionList = $this->getDoctrine()->getRepository(QuestionList::class);
        $repositoryResponse = $this->getDoctrine()->getRepository(Response::class);
        $repositoryQcm = $this->getDoctrine()->getRepository(Qcm::class);
        $repositoryUser = $this->getDoctrine()->getRepository(User::class);

        $session = new Session();
        // Si la session existe
        if (User::checkSession($request, $session)) {
            // Si l'etudiant a repondu a un qcm
            if($request->get('idQcm')) {

                // Recuperation du nombre de question contenue dans le qcm en cours
                $nbQuestions = sizeof($repositoryQuestionList->findBy(['qcm_id' => $request->get('idQcm')]));
                $note = 0;
                $allResponse = $request->request->all();
                $previousQuestionId = 0;

                // On parcours les reponses de l'utilisateur au qcm
                foreach($allResponse as $key => $value) {
                    $goodAnswer = 1;

                    // On separe les valeurs de la requete
                    // $valueTmp[0] correspond à l'id de la question
                    // $valueTmp[1] correspond à l'id de la reponse
                    $valueTmp = explode("-", $key);

                    if($valueTmp[0] != $previousQuestionId) {
                        if(sizeof($valueTmp) == 2) {

                            // Recuperation des reponses dans la bdd de la question en cours
                            $questionTmp = $repositoryResponse->findBy(['question_id' => $valueTmp[0]]);

                            // On parcours les reponses de la bdd
                            foreach($questionTmp as $key2 => $value2) {
                                // On crée un comparateur avec les données de la base, afin de pouvoir les comparer avec les reponses de l'etudiant
                                $valueComparator = $value2->getQuestionId()->getId() . "-" . $value2->getId();

                                // On regarde si la reponse dans la bdd est juste, si on y a pas répondu, alors on retire le point.
                                // On regarde si la reponse dans la bdd est fausse, si on y a repondu, alors on retire le point
                                // Si aucune des conditions n'est verifiee, alors tout va bien, et $goodAnswer reste à 1, alors la reponse est JUSTE
                                if(($value2->getRightAnswer() == '0' && $request->get(strval($valueComparator))) || ($value2->getRightAnswer() == '1' && !$request->get(strval($valueComparator)))) {
                                    $goodAnswer = 0;
                                }
                            }

                            // Si goodanswer == 1 alors on incremente la note
                            if($goodAnswer == 1) {
                                $note++;
                            }
                        }
                    }
                    $previousQuestionId = $valueTmp[0];
                }

                // Formattage de la note sur 20 avec 2 décimales
                $noteSur20 = number_format(($note / $nbQuestions)*20, 2);

                $result = new Result();
                $result->setQcmId($repositoryQcm->find($request->get('idQcm')));
                $result->setResult($noteSur20);
                $result->setUserId($repositoryUser->find($request->getSession()->get('idUser')));
                $result->setVisible(0);
                $entityManager->persist($result);
                $entityManager->flush();

                if($request->getSession()->get('role') == 'Etudiant') {
                    $repositoryQcms = $this->getDoctrine()->getRepository(Qcm::class);
                    $repositoryUser = $this->getDoctrine()->getRepository(User::Class);
                    $repositoryResult = $this->getDoctrine()->getRepository(Result::class);

                    $idUser = $repositoryUser->find($request->getSession()->get('idUser'));
                    $qcms = $repositoryQcms->findBy(
                        ['visible' => '1']
                    );
                    $arrayAnsweredQuestions = array();
                    foreach($qcms as $key => $value) {
                        if($repositoryResult->findBy(['user_id' => $idUser, 'qcm_id' => $value->getId()])) {
                            array_push($arrayAnsweredQuestions, $qcms[$key]);
                        }
                        if($value->getDeadline() < new \DateTime() || $repositoryResult->findBy(['user_id' => $idUser, 'qcm_id' => $value->getId()])) {
                            unset($qcms[$key]);
                        }
                    }
                    $createdMessage = "";
                    $userRole = $request->getSession()->get('role');
                    return $this->render('/teacher/showQcms.html.twig', [
                        "qcms" => $qcms,
                        "answeredQcms" => $arrayAnsweredQuestions,
                        "createdMessage" => $createdMessage,
                        'userRole' => $userRole
                    ]);
                }
            }
        } else {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * Affichage des questions/réponses à modifier
     * @Route("/updateQcm/{idQcm}", name="updateQcm")
     * @param int $idQcm
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateQcm($idQcm, Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session)) {
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(QuestionList::class);
                $repositoryQuestions = $this->getDoctrine()->getRepository(Question::class);

                $qcm= $repository->findBy(['qcm_id' => $idQcm]);
                $questions = $repositoryQuestions->findAll();
                $userRole = $request->getSession()->get('role');

                $datas = array("qcmQuestions" => $qcm, "questions" => $questions, 'userRole' => $userRole);
                return $this->render('/teacher/updateQcm.html.twig', $datas);
            } else {
                $this->get('session')->clear();

                $this->get('session')->clear();

                return $this->redirect('/');
            }
        } else {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }

    }
    /**
     * Affichage des résultats des étudiants
     * @Route("/showResults", name="showResults")
     */
    public function showResults(Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session)) {
            $userRole = $request->getSession()->get('role');
            $idUser = $request->getSession()->get('idUser');
            if($userRole == 'Enseignant') {
                if($request->get('publishResult')) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $result = $entityManager->getRepository(Result::class)->find($request->get('publishResult'));
                    $result->setVisible(1);
                    $entityManager->flush();
                }

                $repositoryResult = $this->getDoctrine()->getRepository(Result::class);
                $results = $repositoryResult->findAll();
                $datas = array("userRole" => $userRole, "results" => $results);

                return $this->render('/student/results.html.twig', $datas);
            } else if($userRole == 'Etudiant') {
                $repositoryResult = $this->getDoctrine()->getRepository(Result::class);

                $results = $repositoryResult->findBy(
                    ['visible' => '1', 'user_id' => $idUser]
                );
                $createdMessage = "";
                return $this->render('/student/results.html.twig', [
                    "results" => $results,
                    "createdMessage" => $createdMessage,
                    'userRole' => $userRole,
                    'idUser' => $idUser
                ]);
            } else {
                $user = $this->getDoctrine()->getRepository(User::Class)->findOneByLogin($session->get('username'));

                $userInf =
                    array('username' => $user->getLogin(),
                        'password' => $user->getPassword(),
                        'role' => $user->getRole(),
                        'email' => $user->getEmail(),
                        'userRole' => $userRole);

                return $this->render('teacher/test.html.twig', $userInf);
            }
        } else {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }

    }

    /**
     * Insert dans la base de données le nouveau qcm et la liste de questions qu'il contient
     * @Route("/createQcm", name="CreateQcm")
     */
    public function createQcm(Request $request)
    {
        $session = new Session();

        if(User::checkSession($request, $session))
        {
            $userRole = $request->getSession()->get('role');
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(Question::class);
                $questions = $repository->findAll();
                if($request->get('idQuestion') === null) {
                    return $this->render('/teacher/createQcm.html.twig', [
                        "questions" => $questions
                    ]);
                } else {
                    $questionsRequest = $request->get('idQuestion');
                    $user = $this->getDoctrine()->getRepository(User::Class)->findOneByLogin($session->get('username'));

                    $entityManager = $this->getDoctrine()->getManager();

                    // Création du nouveau qcm
                    $qcm = new Qcm();
                    $qcm->setAuthorId($user);
                    $qcm->setDeadline(new \DateTime($request->get('deadline')));
                    $qcm->setVisible(0);
                    $qcm->setTitle($request->get('titre'));
                    $entityManager->persist($qcm);

                    // Création de la liste des questions sélectionnées dans le qcm
                    foreach($questionsRequest as $question) {
                        $questionList = new QuestionList();
                        $questionTmp = $this->getDoctrine()->getRepository(Question::Class)->find($question);
                        $questionList->setQuestionId($questionTmp);
                        $questionList->setQcmId($qcm);
                        $entityManager->persist($questionList);
                    }
                    $entityManager->flush();

                    return $this->render('/teacher/createQcm.html.twig', [
                        "questions" => $questions
                    ]);
                }
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        } else {
            $erreur = array('erreur' => "Veullez vous identifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * toString
     * @return string
     */
    public function __toString()
    {
        return $this->getMail();
    }

}