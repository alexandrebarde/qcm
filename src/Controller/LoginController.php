<?php


namespace App\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{

    /**
     * Vérifie l'identifiant et le mot de passe fournit dans le formulaire
     * @Route("/login", name="login")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $erreur = array('erreur' => "");
        $session = new Session();

        // Si on est déjà connecté, on redirige vers la page d'accueil avec les données de l'utilisateur connecté
        if(User::checkSession($request, $session))
        {
            $userRole = $request->getSession()->get('role');
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository(User::Class)->findOneByLogin($session->get('username'));
            $userInf =
                array('username' => $user->getLogin(),
                    'password' => $user->getPassword(),
                    'role' => $user->getRole(),
                    'email' => $user->getEmail(),
                    'userRole' => $userRole);
            return $this->render('/teacher/test.html.twig', $userInf);
        }
        else
        {
            // Récupération des éléments du formulaire de l'utilisateur
            $username = $request->get('username');
            $password = $request->get('password');

            // Création d'un tableau associatif pour stocker les informations de l'utilisateur
            $userInf = array('username' => $username, 'password' => $password);

            // Vérification de l'existence des données de l'utilisateur dans la requête
            // Si il n'a pas remplit tous les champs, on renvoi un message d'erreur
            if(isset($username) && isset($password) && !empty($username) && !empty($password))
            {
                $user = $this->getDoctrine()->getRepository(User::Class)->findOneByLogin($username);
                // Comparaison du mot de passe de l'utilisateur qui tente de se connecter avec la base de données
                if($user !== null) {
                    if(hash('sha256', ($password)) === $user->getPassword())
                    {
                        $session->set('idUser', $user->getId());
                        $session->set('username', $username);
                        $session->set('password', $password);
                        $session->set('email', $user->getEmail());
                        $session->set('role', $user->getRole());
                        $session->set('etat', "Connexion");
                        $userInf['etat'] = "Connexion ok";
                        return $this->redirect('/');
                    }
                    else
                    {
                        $erreur['erreur'] = "Mot de passe incorrect";
                        return $this->render('home.html.twig', $erreur);
                    }
                } else {
                    return $this->render('home.html.twig', $erreur);
                }
            }
            else
            {
                $erreur['erreur'] = "Il manque des champs";
                return $this->render('home.html.twig', $erreur);
            }
        }
    }

    /**
     * @Route("/logout", name="logoutController")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * Supprime la session existante et redirige vers la page d'accueil
     */
    public function logout(Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session))
        {
            $this->get('session')->clear();
        }
        $erreur = array('erreur' => "");
        return $this->render('home.html.twig', $erreur);
    }


}